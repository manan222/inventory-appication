<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::prefix('admin')->group(function()
 {
   Route::get('/dashboard','AdminController@dashboard')->name('admindashboard');
   Route::get('/edit/profile','AdminController@editprofile')->name('editprofile');
   Route::get('/role','AdminController@managerole')->name('managerole');
 }
 );
Route::prefix('transaction')->group(function()
 {
    Route::get('/new','TransactionController@index')->name('newTransaction');
    Route::post('/new/post','TransactionController@addTransaction')->name('storeTransaction');
    Route::get('/history','TransactionController@getAllTransactions')->name('historyTransaction');
    Route::get('edit/{id}','TransactionController@editTransaction')->name('editTransaction');
    Route::post('update/{id}','TransactionController@updateTransaction')->name('updateTransaction');
 });
 Route::prefix('expense')->group(function()
 {
    Route::get('/new','ExpenseController@addExpense')->name('addExpense');
    Route::post('/new/post','ExpenseController@addExpensePost')->name('addExpensePost');
    Route::get('edit/{id}','ExpenseController@editExpense')->name('editExpense');
    Route::post('update/{id}','ExpenseController@updateExpense')->name('updateExpense');
    Route::get('delete/{id}','ExpenseController@deleteExpense')->name('deleteExpense');
 });
 Route::prefix('stock')->group(function()
 {
    Route::get('/','StockController@stock')->name('managestock');
    Route::post('/store','StockController@createStock')->name('createStock');
    Route::get('edit/{id}','StockController@editStock')->name('editStock');
    Route::post('update/{id}','StockController@updateStock')->name('updateStock');
    Route::get('delete/{id}','StockController@deleteStock')->name('deleteStock');
 });
 Route::prefix('menu')->group(function()
 {
    Route::get('/','AdminController@menu')->name('managemenu');
    Route::post('/menu/store','AdminController@menustore')->name('menuStore');
 });
 Route::prefix('user')->group(function()
 {
    Route::get('/add','UserController@addUser')->name('addUser');
    Route::post('/store','UserController@storeUser')->name('storeUser');
    Route::get('/edit/{id}','UserController@editUser')->name('editUser');
    Route::post('/update/{id}','UserController@updateUser')->name('updateUser');
    Route::get('/delete/{id}','UserController@deleteUser')->name('deleteUser');
 });


Route::prefix('customers')->group(function()
 {
    Route::get('add/','CustomerController@index')->name('addCustomer');
    Route::post('add/post','CustomerController@store')->name('addNewCustomer');
    Route::get('edit/customer/{id}','CustomerController@editCustomer')->name('editCustomer');
    Route::post('update/customer/{id}','CustomerController@updateCustomer')->name('updateCustomer');
    Route::get('delete/customer/{id}','CustomerController@deleteCustomer')->name('deleteCustomer');
});
Auth::routes();   

Route::get('/', 'AdminController@dashboard')->name('admindashboard');
