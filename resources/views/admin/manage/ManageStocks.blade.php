@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="{{ route('createStock') }}" method="POST" class="form-horizontal form-label-left" novalidate>
                  @csrf
                    <span class="section">Manage Currency <small>Add | Edit Currency</small></span>
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Code<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-6 col-xs-12 mt-2">
                                    <select name="code" class="form-control " aria-placeholder="select">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
             
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Amount in Stock <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" autoComplete="off" id="amount" required="required" name="amount" class="form-control col-md-7 col-xs-12" placeholder="1000">
                  </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">In Local Currency <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12 mt-2">
                            <select name="is_local" class="form-control " aria-placeholder="select">
                                {{--  @foreach($currencies as $currency)  --}}
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                                {{--  @endforeach  --}}
                            </select>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Add Currency</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
                    <span class="section">Available Stock <small>Currencies</small> </span>
            </div>
            <div class="x_content">
                <p>This is the existing currency amount you have</p>
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>SR.No</th>
                    <th>Code</th>
                    <th>Country</th>
                    <th>Currency</th>
                    <th>Available Amount</th>
                    <th>In Local Currency</th>
                    <th>Last Updated</th>
                    <th>Action</th>
                  </tr>
                </thead>


                <tbody>
                @foreach($stocks as $stock)
                  <tr>
                    <td>{{ $stock->currency->id }}</td>
                    <td>{{ $stock->currency->code }}</td>
                    <td>{{ $stock->currency->country }}</td>
                    <td>{{ $stock->currency->currency }}</td>
                    <td>{{ $stock->available_amount }}</td>
                    <td>{{ $stock->local_currency }}</td>
                    <td>{{\Carbon\Carbon::parse($stock->updated_at)->diffForhumans()}}</td>
                    <td><a href="{{route('editStock',$stock->id)}}" class="btn btn-primary">Update</a>
                    <a href="{{route('deleteStock',$stock->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
</div>
  </div>

@endsection
