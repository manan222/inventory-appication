@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="{{ route('updateStock',$stock->id) }}" method="POST" class="form-horizontal form-label-left" novalidate>
                  @csrf
                    <span class="section">Manage Currency <small>Update Currency</small></span>
                    <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Code<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-6 col-xs-12 mt-2">
                                    <select name="code" class="form-control " aria-placeholder="select">
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                            <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Amount in Stock <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" name="available_amount" value="{{$stock->available_amount}}" autoComplete="off" id="amount" required="required" class="form-control col-md-7 col-xs-12" placeholder="1000">
                  </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">In Local Currency <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12 mt-2">
                            <select name="is_local" class="form-control " aria-placeholder="select">
                                {{--  @foreach($currencies as $currency)  --}}
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                                {{--  @endforeach  --}}
                            </select>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Update</button>
                    <a href="{{route('managestock')}}"   class="btn btn-danger" >Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    
</div>
  </div>

@endsection
