@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">
      {{--  <div class="page-title">
        <div class="title_left">
          <h3>Form Validation</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>  --}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form  action="{{ route('menuStore') }}" method="POST" class="form-horizontal form-label-left" novalidate>
                  @csrf
                    <span class="section">Manage Menu <small>Add | Update Menu</small></span>
                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="name"> Label <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text"  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"
                     name="label"  required="required" type="text" placeholder="Label">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Link <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" id="email" name="link" required="required"
                    class="form-control col-md-7 col-xs-12" placeholder="Link">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Sort <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="email" id="email2" name="sort" data-validate-linked="email"
                    required="required" class="form-control col-md-7 col-xs-12" placeholder="Sort">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Icon <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" id="amount" required="required" name="icon   "
                     class="form-control col-md-7 col-xs-12" placeholder="Icon">
                  </div>
                </div>
                <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Parent <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12 mt-2">
                            <select name="parent" class="form-control ">
                                    <option value="Dashboard"> Dashbaord</option>
                                    <option value="Transaction"> Transaction</option>
                                    <option value="Expense"> Expense</option>
                                    <option value="Manage"> Manage</option>
                            </select>
                        </div>
                </div>
                <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Visibility <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12 mt-2">
                                <select name="visibility" class="form-control ">
                                        <option value="1"> Yes</option>
                                        <option value="0"> No</option>
                                </select>
                        </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Add Menu</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
                    <span class="section">Menu <small>Menu List</small> </span>
            </div>
            <div class="x_content">
                <p>This is the existing currency amount you have</p>
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>SR.No</th>
                    <th>Label</th>
                    <th>Link</th>
                    <th>Parent</th>
                    <th>Sort</th>
                    <th>Icon</th>
                    <th>Show in Menu</th>
                    <th>Action</th>
                  </tr>
                </thead>


                <tbody>
                @foreach($menus as $menu)
                  <tr>
                    <td>{{ $menu->id }}</td>
                    <td>{{ $menu->label }}</td>
                    <td>{{ $menu->link }}</td>
                    <td>{{ $menu->parn }}</td>
                    <td>{{ $menu->sort }}</td>
                    <td>{{ $menu->icon }}</td>
                    <td>{{ $menu->visibility }}</td>
                    <td><button href="#" class="btn btn-primary">Update</button>
                        <button href="#" class="btn btn-danger">Delete</button>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
</div>
  </div>

@endsection
