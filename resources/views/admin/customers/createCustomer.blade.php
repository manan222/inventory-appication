@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div >
      {{--  <div class="page-title">
        <div class="title_left">
          <h3>Form Validation</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>  --}}

    <div class="row" style="background: white">
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="{{ Route('addNewCustomer')}}" id="#update-customer"  class="form-horizontal form-label-left" method="Post">
                @csrf
                <span class="section">Manage Customers <small>Add | Update Customer</small></span>
                <div class="item form-group mt-3">
                  <label class=" control-label col-md-3 col-sm-3 col-xs-12" for="name"> Name <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12 ml-5  ">
                    <input id="text" autoComplete="off"  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"
                     name="name"  required="required" type="text" placeholder="Name">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Contact <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" autoComplete="off" id="email" name="contact" required="required"
                    class="form-control col-md-7 col-xs-12" placeholder="Contact">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="email" autoComplete="off" name="email" data-validate-linked="email"
                    required="required" class="form-control col-md-7 col-xs-12" placeholder="Email">
                  </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">CNIC <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <input type="text" autoComplete="off"  name="cnic" data-validate-linked="email"
                      required="required" class="form-control col-md-7 col-xs-12" placeholder="CNIC">
                    </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">CNIC Expiry <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="date" id="textarea" required="required" name="cnic_expiry"
                     class="form-control col-md-7 col-xs-12" placeholder="CNIC Expiry"></input>
                  </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Address <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <textarea  id="textarea" autoComplete="off" required="required" name="address"
                       class="form-control col-md-7 col-xs-12" placeholder="Address" rows="10" cols="33"></textarea>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3 ">
                    <button id="send" type="submit" class="btn btn-success">Add Customer</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
                    <span class="section">Customers <small> Customer List</small>  </span>
              {{--  <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>  --}}
              {{--  <div class="clearfix"></div>  --}}
            </div>
            <div class="x_content">
                <p>This is the list of all customers</p>
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>SR.No</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>CNIC</th>
                    <th>CNIC Expiry</th>
                    <th>Address</th>
                    <th>Created Date</th>
                    <th>Action</th>
                  </tr>
                </thead>


                <tbody>
                @foreach($customers as $customer)
                  <tr>
                    <td>{{ $customer->id }}</td>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->contact }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->cnic }}</td>
                    <td>{{ $customer->cnic_expiry }}</td>
                    <td>{{ $customer->address }}</td>
                    <td>{{ \Carbon\Carbon::parse($customer->created_at)->diffForhumans() }}</td>
                    <td><a href="{{ route('editCustomer', $customer->id) }}"  class="btn btn-primary">Update</a>
                    <!-- <a href="{{ route('deleteCustomer', $customer->id) }}"  class="btn btn-danger">Delete</a> -->
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
</div>
  </div>

@endsection
