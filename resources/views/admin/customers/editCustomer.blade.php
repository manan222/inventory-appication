@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div >
      {{--  <div class="page-title">
        <div class="title_left">
          <h3>Form Validation</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>  --}}

    <div class="row" style="background: white">
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="{{ Route('updateCustomer',$customer->id)}}" id="#update-customer"  class="form-horizontal form-label-left" method="Post">
                @csrf
                <span class="section">Manage Customers <small>Add | Update Customer</small></span>
                <div class="item form-group mt-3">
                  <label class=" control-label col-md-3 col-sm-3 col-xs-12" for="name"> Name <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12 ml-5  ">
                    <input id="number"  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"
                     name="name"  required="required" type="text" placeholder="Name" value="{{ $customer->name }}">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Contact <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" id="email" name="contact" required="required"
                    class="form-control col-md-7 col-xs-12" placeholder="Contact" value="{{ $customer->contact }}">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="email" name="email" data-validate-linked="email"
                    required="required" class="form-control col-md-7 col-xs-12" placeholder="Email" value="{{ $customer->email }}" >
                  </div>
                </div>
                <div class="item form-group"> 
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">CNIC <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <input type="text"  name="cnic" data-validate-linked="email"
                      required="required" class="form-control col-md-7 col-xs-12" placeholder="CNIC" value="{{ $customer->cnic}}">
                    </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">CNIC Expiry <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="date" id="textarea" required="required" name="cnic_expiry"
                     class="form-control col-md-7 col-xs-12" placeholder="CNIC Expiry" value="{{ $customer->cnic_expiry }}">
                  </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Address <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <textarea  id="textarea" required="required" name="address" "
                       class="form-control col-md-7 col-xs-12" placeholder="Address" rows="10" cols="33">{{ $customer->address }}</textarea>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3 ">
                    <button id="send" type="submit" class="btn btn-success">Update</button>
                    <a href="{{route('addCustomer')}}" class="btn btn-danger"> Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
  </div>

@endsection
