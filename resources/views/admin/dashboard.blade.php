@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
        <div class="row">
          <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats">
                <div class="icon"><i class="fa fa-exchange" aria-hidden="true"></i></div>
                <div class="count">{{ App\transaction::all()->count() }}</div>
                <h3>Total Transactions</h3>
                <p>Transactions performed till date</p>
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <div class="tile-stats">
                <div class="icon"><i class="fa fa-users" aria-hidden="true"></i></div>
                <div class="count">{{ App\Customer::all()->count() }}</div>
                <h3>Total Customers</h3>
                <p>Customers Registered till date</p>
              </div>
            </div>
         
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                  <div class="count">{{ App\User::all()->count() }}</div>
                  <h3>Total Users</h3>
                  <p>Transactions performed till date</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-calculator" aria-hidden="true"></i></div>
                  <div class="count">{{ App\Expense::all()->count() }}</div>
                  <h3>Total Expenses</h3>
                  <p>Transactions performed till date</p>
                </div>
            </div>
          </div>
        </div>
          <div class="row">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
                  <div class="count">{{App\Transaction::where('type','Purchase')->sum('amount_lc') }}</div>
                  <h3>Total Purchases</h3>
                  <p>Customers Registered till date</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                <div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                  <div class="count">{{App\Transaction::where('type','Sale')->sum('amount_lc') }}</div>
                  <h3>Total Sales</h3>
                  <p>Customers Registered till date</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
                  <div class="count">{{ App\Transaction::where('type','Purchase')->count() }}</div>
                  <h3>Purchases</h3>
                  <p>Transactions performed till date</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                  <div class="count">{{ App\Transaction::where('type','sale')->count() }}</div>
                  <h3>Sales</h3>
                  <p>Transactions performed till date</p>
                </div>
            </div>
          </div>
          <div class="row">
            @php
                $total_purchases=App\Transaction::where('type','Purchase')->sum('amount_lc');
                $total_sales=App\Transaction::where('type','Sale')->sum('amount_lc');
                $expenses=App\Expense::all()->sum('amount');
                $available_cash=$total_purchases-$total_sales-$expenses;
            @endphp
            <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                  <div class="count">{{ $available_cash}}</div>
                  <h3>Available Amount</h3>
                  <p>Customers Registered till date</p>
                </div>
            </div>
    
            @php
                $total_purchases=App\Transaction::where('type','Purchase')->sum('amount_lc');
                $total_sales=App\Transaction::where('type','Sale')->sum('amount_lc');
                $expenses=App\Expense::all()->sum('amount');
                $available_cash=$total_purchases-$total_sales-$expenses;
            @endphp
            <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-9">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                  <div class="count">{{ $total_sales *0.05   }}</div>
                  <h3>Profit</h3>
                  <p>Customers Registered till</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></div>
                  <div class="count">{{App\Expense::all()->sum('amount') }}</div>
                  <h3>Total Expenses</h3>
                  <p>Customers Registered till date</p>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-shopping-money" aria-hidden="true"></i></div>
                    <div class="count">{{App\Stock::all()->where('currency_id','4')->sum('available_amount') }}</div>
                    <h3>Euro  Stock</h3>
                    <p>Customers Registered till date</p>
                  </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-shopping-money" aria-hidden="true"></i></div>
                    <div class="count">{{App\Stock::all()->where('currency_id','2')->sum('available_amount') }}$</div>
                    <h3>Dollar  Stock</h3>
                    <p>Customers Registered till date</p>
                  </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-shopping-money" aria-hidden="true"></i></div>
                    <div class="count">{{App\Stock::all()->where('currency_id','6')->sum('available_amount') }}</div>
                    <h3>Iraqi  Stock</h3>
                    <p>Customers Registered till date</p>
                  </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-shopping-money" aria-hidden="true"></i></div>
                    <div class="count">{{App\Stock::all()->where('currency_id','10')->sum('available_amount') }}</div>
                    <h3>Australian  Stock</h3>
                    <p>Customers Registered till date</p>
                  </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-shopping-money" aria-hidden="true"></i></div>
                    <div class="count">{{App\Stock::all()->where('currency_id','5')->sum('available_amount') }}</div>
                    <h3>Kuwait  Stock</h3>
                    <p>Customers Registered till date</p>
                  </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-shopping-money" aria-hidden="true"></i></div>
                    <div class="count">{{App\Stock::all()->where('currency_id','9')->sum('available_amount') }}</div>
                    <h3>UAE Dirham</h3>
                    <p>Customers Registered till date</p>
                  </div>
            </div>
          
        </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Recent Transactions <small>Buy|Sell</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">
                    This is the existing cuurency amount you have
                  </p>
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Exchange</th>
                        <th>Transaction Type</th>
                        <th>Transaction Code</th>
                        <th>Currency FC</th>
                        <th>Amount FC</th>
                        <th>Rate</th>
                        <th>Date</th>
                        <th>Currency LC</th>
                        <th>Amount LC</th>
                        <th>Customer</th>
                        <th>Contact</th>
                      </tr>
                    </thead>


                    <tbody>
                    @foreach($transactions as $transaction)
                 
                      <tr>
                        <td>Stock</td>
                        <td>{{ $transaction->type }}</td>
                        <td>{{ $transaction->id}}</td>
                        <td>{{ $transaction->currency->code}}</td>
                        <td>{{ $transaction->amount_fc }}</td>
                        <td>{{ $transaction->rate }}</td>
                        <td>{{ $transaction->created_at }}</td>
                        <td>{{ $transaction->currency->code }}</td>
                        <td>{{ $transaction->amount_lc }}</td>
                        <td>{{ $transaction->customer->name}}</td>
                        <td>{{ $transaction->customer->contact}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      </div>


        
      </div>

        
        
  
  

     
@endsection
