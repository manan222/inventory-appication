@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main">
    <div >
      {{--  <div class="page-title">
        <div class="title_left">
          <h3>Form Validation</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>  --}}

    <div class="row" style="background: white">
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form class="form-horizontal form-label-left" novalidate>
                    <span class="section">Account Settings</span>
                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Name <span class="required"></span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12 ml-5">
                    <input id="number"  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"
                     name="name" value={{Auth::user()->username}}  required="required" type="text" placeholder="Name">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="email">Email Address <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="email" id="email" name="email" required="required" value={{Auth::user()->email}}
                    class="form-control col-md-7 col-xs-12" placeholder="Email">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="password" id="email2" name="password" data-validate-linked="email"
                    required="required" value="{{Auth::user()->password}}" class="form-control col-md-7 col-xs-12" placeholder="Password">
                  </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password Confirmation <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <input type="Password" id="email2" name="confirm_password" data-validate-linked="email"
                      required="required"  value="{{Auth::user()->password}}" class="form-control col-md-7 col-xs-12" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3 ">
                    <button id="send" type="submit" class="btn btn-success">Save Changes</button>
                    <a id="back" href="{{ route('admindashboard')}}" class="btn btn-primary">Back</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
  </div>

@endsection
