@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">
      {{--  <div class="page-title">
        <div class="title_left">
          <h3>Form Validation</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                        <a class="btn btn-default" type="a">Go!</a>
                    </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>  --}}

    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="{{ route('storeUser') }}" method="Post" class="form-horizontal form-label-left" novalidate>
                  @csrf
                    <span class="section">Manage <small> Add | Update User</small></span>
                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Username <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="name" autoComplete="off"  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"
                        name="username" required="required" type="text" placeholder="username">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" autoComplete="off" id="email2" name="email" data-validate-linked="email"
                    required="required" class="form-control col-md-7 col-xs-12" placeholder="Email">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" id="email" name="password" required="required"
                    class="form-control col-md-7 col-xs-12" placeholder="Password">
                  </div>
                </div>
               
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Save</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">

                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <form class="form-horizontal form-label-left" novalidate>
                      <span class="section">Manage Rights <small> User Rights</small></span>
                        <div class="item form-group mt-3 ml-5">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="email">Select Roles <span class="required">*</span>
                            </label>
                            <select name="Roles" class="control-label col-md-6 col-sm-3 col-xs-12" >
                                <option value="volvo">Admin</option>
                                <option value="saab">Author</option>
                                <option value="fiat">User</option>
                                <option value="audi">Accountant</option>
                            </select>
                        </div>
                        <div class="item form-group mt-3">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="name"> Menu Items
                            </label>
                            <label class="control-label col-md-6 col-sm-3 col-xs-12" for="name"> Sub Menu Items
                            </label>
                        </div>
                        <div class="col-md-6">
                            <div class="item form-group">
                                <div >
                                <input type="checkbox" id="email" name="email" required="required">Dashbaord
                                </div>
                            </div>
                            <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Transactions

                            </div>
                            <div class="item form-group">
                                <input type="checkbox" id="email" name="email" required="required">Expense
                            </div>
                            <div class="item form-group">
                                <input type="checkbox" id="email" name="email" required="required">Manage
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="item form-group">
                                    <div >

                                    </div>
                                </div>
                                <div class="item form-group mt-4">
                                        <input type="checkbox" id="email" name="email" required="required">Add New
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">History
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Add New
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Add New
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Users
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Customers
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Stock
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Menu
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Roles
                                </div>
                        </div>
                </form>
              </div>
            </div>
          </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
                    <span class="section">Users List </span>
              {{--  <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="a" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>  --}}
              {{--  <div class="clearfix"></div>  --}}
            </div>
            <div class="x_content">
                <p>This is the list of registered users</p>
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>SR.No</th>
                    <th>UserName</th>
                    <th>Email</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Action</th>
                  </tr>
                </thead>


                <tbody>
                @foreach($users as $user)
                  <tr>
                    <td>{{ $user->id}}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{\Carbon\Carbon::parse($user->created_at)->diffForhumans() }}</td>
                    <td>{{\Carbon\Carbon::parse($user->updated_at)->diffForhumans() }}</td>
                    <td><a href="{{route('editUser' ,$user->id)}}" class="btn btn-primary">Update</a>
                        <a href="{{route('deleteUser' ,$user->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
</div>
  </div>

@endsection
