@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">


    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="{{ route('updateUser',$user->id) }}" method="Post" class="form-horizontal form-label-left" novalidate>
                  @csrf
                    <span class="section">Edit | <small>User</small></span>
                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Username <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="name" autoComplete="off" value="{{$user->username}}"  class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"
                        name="username" required="required" type="text" placeholder="username">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password"value="{{$user->password}}" id="email" name="password" required="required"
                    class="form-control col-md-7 col-xs-12" placeholder="Password">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" value="{{$user->email}}" autoComplete="off" id="email2" name="email" data-validate-linked="email"
                    required="required" class="form-control col-md-7 col-xs-12" placeholder="Email">
                  </div>
                </div>
                <div class="item form-group mt-3">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Roles <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="role_type" class="form-control col-md-7 col-xs-12">
                                  <option value="Admin">Admin</option>
                                  <option value="Author">Author</option>
                                  <option value="Accountant">Accountant</option>
                              </select>
                        </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Update</button>
                    <a href="{{route('addUser')}}" class="btn btn-danger">Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">

                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <form class="form-horizontal form-label-left" novalidate>
                      <span class="section">Manage Rights <small> User Rights</small></span>
                        <div class="item form-group mt-3 ml-5">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="email">Select Roles <span class="required">*</span>
                            </label>
                            <select name="Roles" class="control-label col-md-6 col-sm-3 col-xs-12" >
                                <option value="volvo">Admin</option>
                                <option value="saab">Author</option>
                                <option value="fiat">User</option>
                                <option value="audi">Accountant</option>
                            </select>
                        </div>
                        <div class="item form-group mt-3">
                            <label class="control-label col-md-4 col-sm-3 col-xs-12" for="name"> Menu Items
                            </label>
                            <label class="control-label col-md-6 col-sm-3 col-xs-12" for="name"> Sub Menu Items
                            </label>
                        </div>
                        <div class="col-md-6">
                            <div class="item form-group">
                                <div >
                                <input type="checkbox" id="email" name="email" required="required">Dashbaord
                                </div>
                            </div>
                            <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Transactions

                            </div>
                            <div class="item form-group">
                                <input type="checkbox" id="email" name="email" required="required">Expense
                            </div>
                            <div class="item form-group">
                                <input type="checkbox" id="email" name="email" required="required">Manage
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="item form-group">
                                    <div >

                                    </div>
                                </div>
                                <div class="item form-group mt-4">
                                        <input type="checkbox" id="email" name="email" required="required">Add New
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">History
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Add New
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Add New
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Users
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Customers
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Stock
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Menu
                                </div>
                                <div class="item form-group">
                                    <input type="checkbox" id="email" name="email" required="required">Roles
                                </div>
                        </div>
                </form>
              </div>
            </div>
          </div>
    </div>
</div>
  </div>

@endsection
