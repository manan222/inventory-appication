@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>

                 @if(Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="x_content">

              <form action="{{ route('updateExpense',$expense->id) }}" method="POST" class="form-horizontal form-label-left" novalidate>
                    @csrf
                    <span class="section">Expense | <small>Edit</small></span>

                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Category <span class="required">*</span>
                  </label>
                  <div class=" col-md-3 col-sm-6 col-xs-12">
                    <input id="text" value="{{$expense->category}}"  name="category" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name"  required="required" type="text">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Amount <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="number" value="{{$expense->amount}}" id="email" name="amount" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Date <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="date" value="{{$expense->date}}"  name="date" data-validate-linked="email" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Remarks <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <textarea id="textarea"  name="remarks" required="required"  class="form-control col-md-7 col-xs-12" rows="6" cols="33">{{$expense->remarks}}</textarea>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Update</button>
                    <a href="{{route('addExpense')}}" class="btn btn-danger"> Cancel</a>
                 
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    
      </div>
</div>
  </div>

@endsection
