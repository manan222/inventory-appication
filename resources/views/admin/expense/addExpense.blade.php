@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">

              <div class="clearfix"></div>
            </div>

                 @if(Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <div class="x_content">

              <form action="{{ route('addExpensePost') }}" method="POST" class="form-horizontal form-label-left" novalidate>
              @csrf
                    <span class="section">Expene <small>Add | Update</small></span>

                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Category <span class="required">*</span>
                  </label>
                  <div class=" col-md-3 col-sm-6 col-xs-12">
                    <input id="text" autoComplete="off"  name="category" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name"  required="required" type="text">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Amount <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="number" autoComplete="off" id="email" name="amount" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Date <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="date"  name="date" data-validate-linked="email" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Remarks <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <textarea id="textarea"  name="remarks" required="required"  class="form-control col-md-7 col-xs-12" rows="6" cols="33"></textarea>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Add Expense</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Expense <small>Expense History</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>This is the list of all expenses till date</p>
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>SR.No</th>
                    <th>Category</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Remarks</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Actions</th>
                  </tr>
                </thead>


                <tbody>
                    @foreach($expenses as $expense)
                    <tr>
                      <td>{{ $expense->id }}</td>
                      <td>{{ $expense->category }}</td>
                      <td>{{ $expense->amount }}</td>
                      <td>{{ $expense->date }}</td>
                      <td>{{ $expense->remarks }}</td>
                      <td>{{ \Carbon\Carbon::parse($expense->created_at)->diffForhumans() }}</td>
                      <td>{{ \Carbon\Carbon::parse($expense->updated_at)->diffForhumans() }}</td>
                      <td><a href="{{route('editExpense',$expense->id)}}" class="btn btn-primary">Update</a>
                      <a href="{{route('deleteExpense',$expense->id)}}" class="btn btn-danger">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
</div>
  </div>

@endsection
