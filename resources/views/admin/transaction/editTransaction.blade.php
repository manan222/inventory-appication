@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
    <div class="">
      {{--  <div class="page-title">
        <div class="title_left">
          <h3>Form Validation</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>  --}}

      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 col-sm-12 col-xs-12 ">
          <div class="x_panel">
            <div class="x_title">
              {{--  <h2>Form validation <small>sub title</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>  --}}
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form method="post" action="{{ route('updateTransaction',$transaction->id)}}" method="POST" class="form-horizontal form-label-left">
                @csrf
                <span class="section">Transaction | <small>Edit</small></span>
           
                <div class="item form-group mt-3">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Type <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <select name="type" class="form-control col-md-7 col-xs-12">
                            if($transaction->type=="Purchase")
                            {
                               <option value="{{$transaction->type }}"></option>$transaction->type</option>
                            }
                           
                            <option value="Purchase">Purchase</option>
                        </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Currency FC <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="currency_fc" class="form-control col-md-7 col-xs-12">
                      @foreach ($currencies as $curr)
                        <option value="{{ $curr->currency_id }}">{{ $curr->code."-".$curr->currency }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Amount FC <span class="required">*</span>
                  </label>
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <input type="text" value="{{$transaction->amount_fc}}"  id="email2" name="amount_fc"  class="form-control num col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Rate<span class="required">*</span>
                  </label>
                  <div class="form -group col-md-6 col-sm-6 col-xs-12">
                    <input type="text"  value="{{$transaction->rate}}" name="rate"  required="required" class="form-control num col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">Currency LC <span class="required">*</span>
                  </label>
                  <div class="control-label col-md-4 col-sm-6 col-xs-12">
                    <select name="currency_lc" class="form-control col-md-7 col-xs-12">
                      @foreach ($currencies as $curr)
                      <option value="{{ $curr->id }}">{{ $curr->code. "-" .$curr->currency }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount LC <span class="required">*</span>
                  </label>
                  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <input id="result"  value="{{$transaction->amount_lc}}"  type="text" name="amount_lc" class="optional form-control  col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="item form-group">
                  <label  class="control-label col-md-3">Datetime</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input  type="date" value="{{$transaction->date}}" name="date" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Remarks <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="textarea" required="required" name="remarks" class="form-control col-md-7 col-xs-12" rows="6" cols="33">{{$transaction->remarks}}</textarea>
                  </div>
                </div>
                <span class="section">Customer <small>Customer Detail</small></span>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Customer Email <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12 mt-2">
                      <select name="c_id" class="form-control ">
                          @foreach($customers as $customer)
                          <option value="{{$customer->id}}">{{ $customer->email }}</option>
                          @endforeach
                      </select>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="send" type="submit" class="btn btn-success">Update</button>
                    <a href="{{route('historyTransaction')}}" class="btn btn-danger"> Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
