@extends('layouts.welcome')
@section('content')
<div class="right_col" role="main" style="background: white">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Recent Transactions <small>Buy|Sell</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">
                    This is the existing currency amount you have
                  </p>
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Transaction Type</th>
                        <th>Customer</th>
                        <th>Foreign Currency </th>
                        <th>Foreign  Amount </th>
                        <th>Rate</th>
                        <th>Local Currency </th>
                        <th>Local Amount </th>
                        <th>Created Time</th>
                        <th>Updated Time</th>

                        
                        <th>Description</th>
                        <th>Actions</th>
                      </tr>
                    </thead>


                    <tbody>
                    @foreach($transactions as $transaction)
                 
                      <tr>
                        <td>{{ $transaction->type }}</td>
                        <td>{{ $transaction->customer->name ?? ''}}</td>
                        <td>{{ $transaction->currency->code }}</td>
                        <td>{{ $transaction->amount_fc   }}</td>
                        <td>{{ $transaction->rate }}</td>
                        <td>PKR</td>
                        <td>{{ $transaction->amount_lc }}</td>
                        <td>{{\Carbon\Carbon::parse($transaction->created_at)->diffForhumans() }}</td>
                        <td>{{\Carbon\Carbon::parse($transaction->updated_at)->diffForhumans() }}</td>
                        <td>{{ $transaction->remarks }}</td>
                        <td><a  href="{{route('editTransaction',$transaction->id)}}" class="btn btn-primary">Update</a></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
