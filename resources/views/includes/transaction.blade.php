<div class="right_col" role="main" style="background: white">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Recent Transactions <small>Buy|Sell</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">
                    This is the existing cuurency amount you have
                  </p>
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Exchange</th>
                        <th>Transaction Type</th>
                        <th>Transaction Code</th>
                        <th>Currency FC</th>
                        <th>Amount FC</th>
                        <th>Rate</th>
                        <th>Date</th>
                        <th>Currency LC</th>
                        <th>Amount LC</th>
                        <th>Customer</th>
                        <th>Contact</th>
                      </tr>
                    </thead>


                    <tbody>
                    @foreach($transactions as $transaction)
                 
                      <tr>
                        <td>Stock</td>
                        <td>{{ $transaction->type }}</td>
                        <td>{{ $transaction->id}}</td>
                        <td>{{ $transaction->currency->code}}</td>
                        <td>{{ $transaction->amount_fc }}</td>
                        <td>{{ $transaction->rate }}</td>
                        <td>{{ $transaction->created_at }}</td>
                        <td>{{ $transaction->currency->code }}</td>
                        <td>{{ $transaction->amount_lc }}</td>
                        <td>{{ $transaction->customer->name}}</td>
                        <td>{{ $transaction->customer->contact}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

