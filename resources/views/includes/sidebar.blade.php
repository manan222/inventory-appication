<div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
          <a href="{{route('admindashboard')}}" class="site_title"><i class="fa fa-paw"></i> <span>Online</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="/assets/images/ali1.jpg" alt="Error" class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
            <h2>{{ Auth::user()->username }}</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->
          <hr>
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu">
                <li><a href="{{ route('admindashboard') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard </a>
                </li>
                <li><a><i class="fa fa-money" aria-hidden="true"></i>Transactions <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('newTransaction') }}">Add New</a></a></li>
                    <li><a href="{{ route('historyTransaction') }}">History</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-money" aria-hidden="true"></i>Expense<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('addExpense') }}">Add New</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-wrench" aria-hidden="true"></i> Manage <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('addUser') }}">Users</a></li>
                    <li><a href="{{ route('addCustomer') }}">Customers</a></li>
                    <li><a href="{{ route('managestock') }}">Stock</a></li>
                    <li><a href="{{ route('managemenu') }}">Menus</a></li>
                    <li><a href="{{ route('managerole') }}">Roles</a></li>
                  </ul>
                </li>
              </ul>
            </div>

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>
