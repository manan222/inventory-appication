<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('currency_fc');
            $table->integer('amount_fc');
            $table->string('currency_lc');
            $table->integer('amount_lc');
            $table->date('datetime');
            $table->string('remarks');
            $table->integer('customer_id');
            $table->foreign('currency_fc')->references('currency')->on('currencies')->onDelete('cascade');
            $table->foreign('currency_lc')->references('currency')->on('currencies')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
