<?php

namespace App\Http\Controllers;
use App\Customer;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    //
    public function index()
    {
        $customers=Customer::orderby('id','desc')->get();
        return view('admin.customers.createCustomer',compact('customers'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
        'name'=>'required|string',
        'contact'=>'required',
        'email' =>'required|string',
    ]);
    $customer=new customer();
    $customer->name=$request['name'];
    $customer->contact=$request['contact'];
    $customer->email=$request['email'];
    $customer->cnic=$request['cnic'];
    $customer->cnic_expiry=$request['cnic_expiry'];
    $customer->address=$request['address'];
    $customer->save();
    return back();
    }
    public function editCustomer($id)
    {
        $customer=Customer::where('id',$id)->first();
        return view('admin.Customers.editCustomer',compact('customer'));
    }
    public function updateCustomer(Request $request,$id)
    {
        $customer=customer::where('id',$id)->first();  //error
        $customer->name=$request['name'];
        $customer->contact=$request['contact'];
        $customer->email=$request['email'];
        $customer->cnic=$request['cnic'];
        $customer->cnic_expiry=$request['cnic_expiry'];
        $customer->address=$request['address'];
        $customer->save();
        return redirect("customers/add");
    }
    public function deleteCustomer($id)
    {
        $customer=customer::where('id',$id)->first(); 
        $customer->delete();
        return back();

    }
}
