<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function addUser()
    {
        $users=User::all();
        return view('admin.user.addUser' ,compact('users'));

    }
    public function storeUser(Request $request)
    {
         $this->validate($request,[
             'username'=>'required|string',
             'password'=>'required|string',
             'email'=> 'required|string'
         ]);
         $user=new user();
         $user->username=$request['username'];
         $user->email=$request['email'];
         $password=$request['password'];
         $user->password=bcrypt($password);
         $user->save();
         return back();
    }
    public function editUser($id)
    {
        $user=User::where('id',$id)->first();
        return view('admin.user.editUser',compact('user'));
    }
    public function updateUser(Request $request,$id)
    {
      $user=User::where('id',$id)->first();  //error
      $user->username=$request['username'];
      $user->email=$request['email'];
      $user->password=$request['password'];
      $user->save();
        return redirect("user/add");
    }
    public function deleteUser($id)
    {
        $user=User::where('id',$id)->first();
        $user->delete();
        return back();
    }
}
