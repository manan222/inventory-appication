<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Menu;
use App\Stock;
use App\Currency;
use App\Customer;
use App\SubMenu;
use App\Transaction;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   public function dashboard()
   {
    $transactions = Transaction::orderby('id','desc')->get();
    $currency=Currency::all();
    $customers=Customer::all();
       return view('admin.dashboard',compact('transactions','currency','customers'));
   }
   public function menu()
   {
    $menus=Menu::all();
    return view('admin.manage.ManageMenu',compact('menus'));
   }
   public function menustore(Request $request)
   {
    $this->validate($request,[
        'label'=>'required|string',
        'link'=>'required',

    ]);
    $menu=new Menu();
    $menu->label=$request['label'];
    $menu->link=$request['link'];
    $menu->sort=$request['sort'];
    $menu->icon=$request['icon'];
    $menu->parn=$request['parent'];
    $menu->visibility=$request['visibility'];
    $menu->save();
    return back();
   }
   public function managerole()
   {
       return view('admin.manage.ManageRole');
   }
   public function editprofile()
   {
       return view('admin.EditProfile');
   }
}
