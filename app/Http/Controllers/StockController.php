<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Currency;
class StockController extends Controller
{
    
    public function stock()
    {
        $currencies=Currency::all();
        $stocks=Stock::orderby('id','desc')->get();
         return view('admin.manage.ManageStocks',compact('currencies','stocks'));
    }
    public function createStock(Request $request)
    {
     $this->validate($request,[
         'amount'=>'required|string',
         'is_local'=>'required|string',
     ]);
     $stock=new stock();
     $stock->currency_id=$request['code'];
     $stock->available_amount=$request['amount'];
     $stock->local_currency=$request['is_local'];
     $stock->save();
     return back();
    }
    public function editStock($id)
    {
        $currencies=Currency::all();
        $stock=Stock::where('id',$id)->first();
        return view('admin.manage.editStock',compact('stock','currencies'));
    }
    public function updateStock(Request $request,$id)
    {   
    $stock=Stock::where('id',$id)->first();  //error
    $stock->currency_id=$request['code'];
    $stock->currency->country =$request['country'];
    $stock->local_currency=$request['is_local'];
    $stock->available_amount=$request['available_amount'];
    $stock->save();
        return redirect("stock");
    }
    public function deleteStock($id)
    {
        $stock=stock::where('id',$id);
        $stock->delete();
        return back();
    }
}
