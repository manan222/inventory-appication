<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Transaction;
use App\Customer;
Use App\Currency;

class TransactionController extends Controller
{
    //
    public function index() //Create new transaction UI
    {
        $currencies=Currency::all(); 
        $customers=Customer::all();
        return view('admin.transaction.NewTransaction',compact('currencies','customers'));
    }
    public function addTransaction(Request $request) //Create new transaction request
    {
        $this->validate($request,[
            // database fields Transactions Table
            'type'=>'required|string',
            'currency_fc'=>'required|string',
            'amount_fc'=>'required|int',
            'amount_lc'=>'required|int',
            'rate'=>'required|string',
            'currency_lc'=>'required|string',
            'remarks' =>'required|string',
        ]);
        $transaction=new Transaction();
            // database-value=input value
        $transaction->type=$request['type'];
        $transaction->currency_id=$request['currency_fc'];
        $transaction->currency_fc=$request['currency_fc'];
        $transaction->amount_fc=$request['amount_fc'];
        $transaction->rate=$request['rate'];
        $transaction->currency_lc=$request['currency_lc'];
        $transaction->amount_lc=$request['amount_lc'];
        $transaction->date=$request['date'];
        $transaction->remarks=$request['remarks'];
        $transaction->customer_id=$request['c_id'];
        $transaction->save();
        return redirect('/transaction/history');
    }
    public function getAllTransactions()   //Get all transaction UI 
    {
        $transactions = Transaction::orderby('id','desc')->get();
        $currency=Currency::all();
        $customers=Customer::all();
        return view('admin.transaction.TransactionHistory',compact('transactions','currency','customers'));
    }
    public function editTransaction($id)  //Edit Transaction UI
    {
        $currency=Currency::where('id',$id)->first();
        $currencies=Currency::all();
        $customers=Customer::all();
        $transaction=Transaction::where('id',$id)->first();
        echo "transaction object=",$transaction;
        return view('admin.transaction.editTransaction',compact('transaction','currencies','customers'));
    }
    public function updateTransaction(Request $request,$id)  //Update Transaction Request
    {
      $transaction=Transaction::where('id',$id)->first();  //error
      $transaction->type=$request['type'];
      $transaction->currency_id=$request['currency_fc'];
      $transaction->currency_fc=$request['currency_fc'];
      $transaction->amount_fc=$request['amount_fc'];
      $transaction->rate=$request['rate'];
      $transaction->currency_lc=$request['currency_lc'];
      $transaction->amount_lc=$request['amount_lc'];
      $transaction->date=$request['date'];
      $transaction->remarks=$request['remarks'];
      $transaction->customer_id=$request['c_id'];
      $transaction->save();
        return redirect("transaction/history");
    }
}
