<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;

class ExpenseController extends Controller
{
   public function addExpense()
   {
        $expenses=Expense::orderby('id','desc')->get();
       return view('admin.expense.addExpense' ,compact('expenses'));

   }
   public function addExpensepost(Request $request)
   {
        $this->validate($request,[
            'category'=>'required|string',
            'amount'=>'required|int',
            'remarks' =>'required|string',
        ]);
        $expense=new expense();
        $expense->category=$request['category'];
        $expense->amount=$request['amount'];
        $expense->date=$request['date'];
        $expense->remarks=$request['remarks'];
        $expense->save();
        return back();
   }
   public function editExpense($id)
   {
       $expense=Expense::where('id',$id)->first();
       return view('admin.expense.editExpense',compact('expense'));
   }
   public function updateExpense(Request $request,$id)
   {
     $expense=Expense::where('id',$id)->first();  //single record
     $expense->category=$request['category'];
     $expense->amount=$request['amount'];
     $expense->date=$request['date'];
     $expense->remarks=$request['remarks'];
     $expense->save();
       return redirect("expense/new");
   }
   public function deleteExpense($id)
   {
       $expense=Expense::where('id',$id)->first();
       $expense->delete();
       return back();
   }

}
